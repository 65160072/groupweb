type OrderItem = {
  id: number
  Name: string
  Amount: number
  unit_price: number
  Total_price: number
}

export { type OrderItem }
