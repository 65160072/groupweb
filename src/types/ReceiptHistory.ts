import type { ReceiptItem } from '@/types/ReceiptItem'
import type { User } from './User'

type HistoryItem = {
  id: number
  createDate: Date
  totalBefore: number
  memberDiscount: number
  total: number
  receivedAmount: number
  change: number
  paymentType: string
  userId: number
  user?: User
  memberId: number
  items: ReceiptItem[]
}

export { type HistoryItem }
