import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Ingredient } from '@/types/Ingredient'

export const useIngredientStore = defineStore('ingredient', () => {
  const ingredients = ref<Ingredient[]>([
    {
      id: 1,
      IngredientID: 'P001',
      IngredientName: 'น้ำตาล(Sugar)',
      Minimum: 10,
      Balance: 25,
      Price: 40
    },
    {
      id: 2,
      IngredientID: 'P002',
      IngredientName: 'วิปครีม(whipped cream)',
      Minimum: 5,
      Balance: 12,
      Price: 289
    },
    {
      id: 3,
      IngredientID: 'P003',
      IngredientName: 'ผงชาเขียว(matcha powder)',
      Minimum: 10,
      Balance: 16,
      Price: 70
    },
    {
      id: 4,
      IngredientID: 'P004',
      IngredientName: 'กาแฟสด(Fresh Coffee)',
      Minimum: 8,
      Balance: 20,
      Price: 90
    },
    {
      id: 5,
      IngredientID: 'P005',
      IngredientName: 'น้ำมะนาว(Lime Juice)',
      Minimum: 12,
      Balance: 18,
      Price: 25
    },
    {
      id: 6,
      IngredientID: 'P006',
      IngredientName: 'นมถั่วเหลือง(Soy Milk)',
      Minimum: 15,
      Balance: 25,
      Price: 60
    },
    {
      id: 7,
      IngredientID: 'P007',
      IngredientName: 'มะม่วงสด(Fresh Mango)',
      Minimum: 10,
      Balance: 15,
      Price: 120
    },
    {
      id: 8,
      IngredientID: 'P008',
      IngredientName: 'น้ำมันมะพร้าว(Coconut Oil)',
      Minimum: 5,
      Balance: 10,
      Price: 50
    },
    {
      id: 9,
      IngredientID: 'P009',
      IngredientName: 'เกลือ(Salt)',
      Minimum: 3,
      Balance: 8,
      Price: 15
    },
    {
      id: 10,
      IngredientID: 'P010',
      IngredientName: 'พริกไทยดำ(Black Pepper)',
      Minimum: 2,
      Balance: 5,
      Price: 18
    },
    {
      id: 11,
      IngredientID: 'P011',
      IngredientName: 'น้ำมันกระเทียม(Garlic Oil)',
      Minimum: 5,
      Balance: 12,
      Price: 30
    },
    {
      id: 12,
      IngredientID: 'P012',
      IngredientName: 'ไข่ไก่(Egg)',
      Minimum: 20,
      Balance: 30,
      Price: 6
    },
    {
      id: 13,
      IngredientID: 'P013',
      IngredientName: 'มิ้นท์เซอร์เวอร์(Mint Server)',
      Minimum: 3,
      Balance: 8,
      Price: 40
    },
    {
      id: 14,
      IngredientID: 'P014',
      IngredientName: 'ถั่วเขียว(Green Beans)',
      Minimum: 10,
      Balance: 18,
      Price: 35
    }
  ])
  const currentIngredient = ref<Ingredient | null>()
  const searchIngredient = (name: string) => {
    const index = ingredients.value.findIndex((item) => item.IngredientName === name)
    if (index < 0) {
      currentIngredient.value = null
    }
    currentIngredient.value = ingredients.value[index]
  }
  function clear() {
    currentIngredient.value = null
  }

  return {
    ingredients,
    currentIngredient,
    searchIngredient,
    clear
  }
})
