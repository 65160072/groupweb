import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const memberDialog = ref(false)
  const members = ref<Member[]>([
    {
      id: 1,
      name: 'ธนพล จิราพร',
      tel: '0123456789',
      point: 100
    },
    {
      id: 2,
      name: 'พีรพัฒน์ สุวรรณโครธ',
      tel: '0954402297',
      point: 270
    },
    {
      id: 3,
      name: 'สิงห์',
      tel: '0989879541',
      point: 10
    },
    {
      id: 5,
      name: 'ญาณากร บรรผนึก',
      tel: '0934781717',
      point: 50
    },
    {
      id: 6,
      name: 'สมหญิง สีแดง',
      tel: '0989999999',
      point: 78
    },
    {
      id: 7,
      name: 'สมศัก มาไกล',
      tel: '09874154474',
      point: 95
    }
  ])
  const tel = ref('')
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }
  function clear() {
    currentMember.value = null
  }

  function saveMember(mem: Member) {
    const newReceipt: Member = {
      id: mem.id,
      name: mem.name,
      tel: mem.tel,
      point: mem.point
    }
    members.value.push(newReceipt)
    clear()
    memberDialog.value = false
  }

  return {
    members,
    currentMember,
    saveMember,
    searchMember,
    clear,
    tel
  }
})
