import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import type { HistoryItem } from '@/types/ReceiptHistory'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    receivedAmount: 0,
    cash: 0,
    change: 0,
    paymentType: 'cash',
    userId: authStore.currentUser.id,
    user: authStore.currentUser,
    memberId: 0
  })
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    console.log(index)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
    }
    const newReceipt: ReceiptItem = {
      id: -1,
      name: product.name,
      price: product.price,
      unit: 1,
      productId: product.id,
      product: product
    }
    receiptItems.value.push(newReceipt)
    calReceipt()
  }
  function removeReceipt(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceipt(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore
    receipt.value.memberId = memberStore.currentMember ? memberStore.currentMember.id : 0
    if (memberStore.currentMember) {
      receipt.value.total = totalBefore * 0.95
      receipt.value.memberDiscount = 5
    } else {
      receipt.value.total = totalBefore
      receipt.value.memberDiscount = 0
    }
  }
  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      cash: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: 0
    }
    memberStore.clear()
  }

  const receiptHistory = ref<HistoryItem[]>([])

  function saveReceipt() {
    const newReceipt: HistoryItem = {
      id: receiptHistory.value.length + 1,
      createDate: new Date(),
      totalBefore: receipt.value.totalBefore,
      memberDiscount: receipt.value.memberDiscount,
      total: receipt.value.total,
      receivedAmount: receipt.value.receivedAmount,
      change: receipt.value.change,
      paymentType: receipt.value.paymentType,
      userId: receipt.value.userId,
      user: receipt.value.user,
      memberId: receipt.value.memberId,
      items: receiptItems.value.map((item) => ({ ...item }))
    }
    receiptHistory.value.push(newReceipt)
    clear()
    receiptDialog.value = false
  }
  return {
    receiptItems,
    receipt,
    receiptDialog,
    receiptHistory,
    addReceiptItem,
    removeReceipt,
    inc,
    dec,
    calReceipt,
    showReceiptDialog,
    clear,
    saveReceipt
  }
})
