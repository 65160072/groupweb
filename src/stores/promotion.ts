import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'

export const usePromotionStore = defineStore('PromotionIn', () => {
  const Promotion = ref<Promotion[]>([
    {
      id: 1,
      name: 'วันพ่อ',
      Discount: '5%'
    }
  ])
  const currentPromotion = ref<Promotion | null>()

  function clear() {
    currentPromotion.value = null
  }

  function savePromotion(pmo: Promotion) {
    const newPromotion: Promotion = {
      id: pmo.id,
      name: pmo.name,
      Discount: pmo.Discount
    }
    Promotion.value.push(newPromotion)
    clear()
  }
  function deletePromotion(pmo: Promotion) {
    currentPromotion
  }
  return {
    Promotion,
    currentPromotion,
    clear,
    savePromotion
  }
})
