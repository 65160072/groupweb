import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Order } from '@/types/Order'
import type { OrderItem } from '@/types/OrderItem'

export const useOrderStore = defineStore('OrderIn', () => {
  const receiptDialog = ref(false)
  const Order = ref<Order[]>([
    {
      id: 1,
      Date: '10/01/2024',
      Amount: 5,
      Total_price: 200,
      Vendor: 'CP'
    },
    {
      id: 2,
      Date: '11/01/2024',
      Amount: 10,
      Total_price: 2889,
      Vendor: 'CP'
    },
    {
      id: 3,
      Date: '12/01/2024',
      Amount: 4,
      Total_price: 280,
      Vendor: 'CP'
    }
  ])
  const OrderItems = ref<OrderItem[]>([])
  const currentOrder = ref<Order | null>()

  function clear() {
    currentOrder.value = null
  }

  function saveOrder(ing: Order) {
    const newOrder: Order = {
      id: ing.id,
      Date: ing.Date,
      Amount: ing.Amount,
      Total_price: ing.Total_price,
      Vendor: ing.Vendor
    }
    Order.value.push(newOrder)
    clear()
  }

  return {
    Order,
    currentOrder,
    clear,
    saveOrder
  }
})
