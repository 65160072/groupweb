import type { Ingredient } from '@/types/Ingredient'
import http from './http'

function addIngredient(ingredient: Ingredient) {
  return http.post('/ingredient', ingredient)
}

function updateIngredient(ingredient: Ingredient) {
  return http.patch('/ingredient/' + ingredient.id, ingredient)
}

function delIngredient(ingredient: Ingredient) {
  return http.delete('/ingredient/' + ingredient.id)
}

function getIngredient(id: number) {
  return http.get('/ingredient/' + id)
}

function getIngredients() {
  return http.get('/ingredient')
}

export default { addIngredient, updateIngredient, delIngredient, getIngredient, getIngredients }
